# README #

WEB SITE handling ALM Solution Licence

### What is this repository for? ###

* Admin side where licences can be created and managed
* Page to produce the encoded string containing the licence for the ALMS software, this page is opened from the ALMS software
* the database is hosted on ALM Solution Server on a MS-SQL DB
* Version 1.0


### Who do I talk to? ###

* Owner: mary.geolin@nitroxconsulting.com
* Backup: olivier.moreau@nitroxconsulting.com