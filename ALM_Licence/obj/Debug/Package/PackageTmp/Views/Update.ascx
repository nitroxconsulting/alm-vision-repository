﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ALM_Licence.Models.Licence>" %>


    <h2 class = "legend">Information [Serial Number : <%: Model.LicenceNumber %>]</h2>
  <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset id="licenceInfo">
            <legend></legend>
            
           <div class="editor-label">
               First Name
            </div>
            <div class="editor-field-j">
                <%: Html.TextBoxFor(model => model.FirstName) %>
                <%: Html.ValidationMessageFor(model => model.FirstName) %>
            </div><br />
          <div class="editor-label">
                Last Name
            </div>
            <div class="editor-field-j">
                <%: Html.TextBoxFor(model => model.LastName) %>
                <%: Html.ValidationMessageFor(model => model.LastName) %>
            </div><br />
            <div class="editor-label">
                Validity Start
            </div>
            <div class="editor-field-j">
            <% if (Model.Start != null){%>
                <%: Html.TextBox("Start",Model.Start.Value.ToShortDateString())%>
                <%: Html.ValidationMessageFor(model => model.Start) %>
                 <script type="text/javascript">
                     $(document).ready(function () {
                         $('#Start').datepicker({
                             changeMonth: true,
                             changeYear: true,
                             dateFormat: 'dd/mm/yy'
                         });
                     }); 
                </script>
            <% } %>
              <% else if (Model.Start == null){%>
                <%: Html.TextBox("Start", Model.Start)%>
                <%: Html.ValidationMessageFor(model => model.Start)%>
                 <script type="text/javascript">
                     $(document).ready(function () {
                         $('#Start').datepicker({
                             changeMonth: true,
                             changeYear: true,
                             dateFormat: 'dd/mm/yy'
                         });
                     }); 
                </script>
             <% } %>
            </div>
            <br />
            <div class="editor-label">
              Validity End
            </div>
            <div class="editor-field-j">
            <% if (Model.End != null){%>
                <%: Html.TextBox("End", Model.End.Value.ToShortDateString())%>
                <%: Html.ValidationMessageFor(model => model.End)%>
                 <script type="text/javascript">
                     $(document).ready(function () {
                         $('#End').datepicker({
                             changeMonth: true,
                             changeYear: true,
                             dateFormat: 'dd/mm/yy'
                         });
                     }); 
                </script>
             <% } %>
             <% else if (Model.End == null){%>
                <%: Html.TextBox("End", Model.End)%>
                <%: Html.ValidationMessageFor(model => model.End)%>
                 <script type="text/javascript">
                     $(document).ready(function () {
                         // $('#End').datepicker({
                         //  changeMonth: true,
                         // changeYear: true,
                         // dateFormat: 'dd/mm/yy'
                         // });
                         alert("show Me");
                     }); 
                </script>
             <% } %>
            </div>
            <br />
            <%: Html.HiddenFor(model => model.Licence_ID) %>
            <%: Html.HiddenFor(model => model.LicenceNumber) %>
            <div class="editor-label">
                Bank ALM
            </div>
            <div class="editor-field-j">
                <%: Html.CheckBox("BankALM")%>
                <%: Html.ValidationMessageFor(model => model.BankALM) %>
            </div>
            <br />
            <div class="editor-label">
               Life Insurance
            </div>
            <div class="editor-field-j">
                <%: Html.CheckBox("LifeInsurance")%>
                <%: Html.ValidationMessageFor(model => model.LifeInsurance) %>
            </div>
            <br />
            <div class="editor-label">
                Pension Fund
            </div>
            <div class="editor-field-j">
                <%: Html.CheckBox("PensionFund")%>
                <%: Html.ValidationMessageFor(model => model.PensionFund) %>
            </div>
            <br />
             <div class="editor-label">
               Porfolio Analysis
            </div>
            <div class="editor-field-j">
                <%: Html.CheckBox("PortfolioAnalysis")%>
                <%: Html.ValidationMessageFor(model => model.PortfolioAnalysis) %>
            </div>
            <br />
            <div class="editor-label">
               Sensitivity Analysis
            </div>
            <div class="editor-field-j">
                <%: Html.CheckBox("SensitivityAnalysis")%>
                <%: Html.ValidationMessageFor(model => model.SensitivityAnalysis) %>
            </div>
            <br />
           
            <div class="buttons">
              &nbsp;<input type="submit" value="Update" />
            </div>
        </fieldset>

    <% } %>



