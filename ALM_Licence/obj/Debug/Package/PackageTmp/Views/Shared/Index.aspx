﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ALM_Licence.Models.ALMEntities>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Update
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <script type="text/javascript">
     (function (i, s, o, g, r, a, m) {
         i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
             (i[r].q = i[r].q || []).push(arguments)
         }, i[r].l = 1 * new Date(); a = s.createElement(o),
             m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
     })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

     ga('create', 'UA-64197541-1', 'auto');
     ga('send', 'pageview');
    </script>

    <fieldset class="updates">
        <legend>Update</legend>
        <div>
        <table class="activate">
        <tr>
            <td> Licence number :</td><td> <%: Html.TextBox("Serial_Nmber", ViewData["serial"], new { disabled = "disabled" })%> </td>
        </tr>
        <tr>
            <td> &nbsp;&nbsp;&nbsp;&nbsp;  </td><td> <strong><%: ViewData["param1"] %> </strong>  </td>
        </tr>
         <tr><td> &nbsp;&nbsp;&nbsp;&nbsp;  </td>
            <td>
            <% if (ViewData["param3"] != null) {%>
                 <% using (Html.BeginForm("Download", "Update",FormMethod.Get))
                   { %>
               
                    <%: Html.Hidden("filename", (string)ViewData["param2"]) %>
                    &nbsp;<input type="image" src="../../Images/dl.PNG" alt="download"/><br />
                    
               
              <% } %>
            <% }%>
            </td>
        </tr>
        <tr>
            <td> &nbsp;&nbsp;&nbsp;&nbsp;  </td><td> <strong><%: ViewData["param2"] %> </strong>  </td>
        </tr>
      </table>
      <br />
      
       </div>
    </fieldset>
</asp:Content>