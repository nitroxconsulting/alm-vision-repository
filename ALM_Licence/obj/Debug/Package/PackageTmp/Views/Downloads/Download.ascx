﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<script type="text/javascript">
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-64197541-1', 'auto');
    ga('send', 'pageview');
</script>
 <head>
    <title>Download Latest Version</title>
 </head>

<div style="color:white;position:absolute;top: 0px;bottom: 0px; width:100%">

   <center>
    <div style="width:100%;height:50%;margin-top:40px">
        <img src="../../Images/banner1.PNG"alt="download" />
    <div style="width:50%;margin-top:-50px;">
         <% using (Html.BeginForm("Download", "Home",FormMethod.Get))
           { %>
               
                 <%: Html.Hidden("filename", (string)ViewData["filename"]) %>
                 &nbsp;
                 <div style="display: inline-block;"><input type="image" src="../../Images/dl.PNG" alt="download"/></div>
                 <div style="color:black">  Latest Version : <strong><%: ViewData["filename"] %></strong></div>
               
      <% } %>
      <hr />
      </div>
        <br />
   </div>
 </center>
</div>