﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<ALM_Licence.Models.LicenceMeta_vw>>" %>
 <script type="text/javascript">
     (function (i, s, o, g, r, a, m) {
         i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
             (i[r].q = i[r].q || []).push(arguments)
         }, i[r].l = 1 * new Date(); a = s.createElement(o),
             m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
     })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

     ga('create', 'UA-64197541-1', 'auto');
     ga('send', 'pageview');
  </script>

  <table class="list">
        <tr>
        <th style="width:13%;"></th>
            <th style="width:12%">
                Serial Number
            </th>
            <th style="width:12%">
                First Name
            </th>
            <th style="width:12%">
               Last Name
            </th>
            <th style="width:10%">
               Company
            </th>
            <th style="width:2%">
               Phone
            </th>
            <th>
               Mail
            </th>
             <th style="width:6%">
               Validity<br /> Start
            </th>
             <th style="width:6%">
               Validity<br /> End
            </th>
            <th style="width:7%">
              <span class="bool-size"> Bank <br />ALM</span>
            </th>
            <th style="width:7%">
              <span class="bool-size"> Life <br />Insurance</span>
            </th>
            <th style="width:7%">
              <span class="bool-size"> Pension<br /> Fund</span>
            </th>
            <th style="width:7%">
              <span class="bool-size">Portfolio <br />Analysis</span>
            </th>
            <th style="width:7%">
               <span class="bool-size">Sensitivity <br />Analysis</span>
            </th>
            <th style="width:10%">
              Last <br />Downloaded <br />Version
            </th>
            <th style="width:10%">
              Last <br />Downloaded <br />Date
            </th>
        </tr>

    <% foreach (var item in Model) { %>
    <%--<% int myId = 0; %>--%>
    
        <tr>
            <td align="center">
                <%: Ajax.ActionLink("Update", "Update", "Admin", new { id = item.Licence_ID },
                new AjaxOptions
                {
                    UpdateTargetId = "update",
                    InsertionMode = InsertionMode.Replace,
                    HttpMethod = "GET"
                },new {@class="updateMe1"})%> |
                <%: Ajax.ActionLink("Delete", "Delete", "Admin", new { id = item.Licence_ID },
                new AjaxOptions
                {
                    UpdateTargetId = "update",
                    InsertionMode = InsertionMode.Replace,
                    HttpMethod = "GET"
                },new {@class="deleteMe"})%>
            </td>
             <td align="center">
                <%: item.LicenceNumber %>
            </td>
            <td align="justify" style="padding-left:2px; padding-right:0px">
                <%: item.FirstName %>
            </td>
            <td align="justify" style="padding-left:2px;">
                <%: item.LastName %>
            </td>
             <td align="justify" style="padding-left:2px;">
                <%: item.metadata_company %>
            </td>
            <td align="left" style="padding-left:2px;">
                <%: item.metadata_phone %>
            </td>
            <td align="justify" style="padding-left:2px;">
                <%: item.metadata_mail %>
            </td>
            <td align="center">
                <% if(item.Start != null){ %>
                <%: item.Start.Value.ToShortDateString() %>
                <% } %>
            </td>
            <td align="center">
                <% if (item.End != null){ %>
               <%: item.End.Value.ToShortDateString() %>
               <% } %>
            </td>
            <td>
                <% if(item.BankALM == true){ %>
               <input type="checkbox" disabled="disabled" checked="checked"/>
                <% }%>
                <% else { %>
                <input type="checkbox" disabled="disabled" />
                <% } %>
            </td>
            <td>
                <% if(item.LifeInsurance == true){ %>
                 <input type="checkbox" disabled="disabled" checked="checked"/>
                <% }%>
                <% else { %>
                <input type="checkbox" disabled="disabled" />
                <% } %>
            </td>
            <td>
                <% if(item.PensionFund == true){ %>
                <input type="checkbox" disabled="disabled" checked="checked"/>
                <% }%>
                <% else { %>
                <input type="checkbox" disabled="disabled" />
                <% } %>
            </td>
            <td>
                <% if(item.PortfolioAnalysis == true){ %>
                <input type="checkbox" disabled="disabled" checked="checked"/>
                <% }%>
                <% else { %>
               <input type="checkbox" disabled="disabled" />
                <% } %>
            </td>
             <td>
                <% if(item.IslamicFinance == true){ %>
                <input type="checkbox" disabled="disabled" checked="checked"/>
                <% }%>
                <% else { %>
                <input type="checkbox" disabled="disabled" />
                <% } %>
            </td>
            <td>
                <%: item.last_Downloaded_Version%>
            </td>
            <td>
                <%: item.last_Downloaded_Date%>
            </td>
        </tr>
    
    <% } %>

    </table>

    <script type ="text/javascript">
        $('.updateMe').click(function () {
            $('#update').modal();
            return false;
        });
        $('.deleteMe').click(function () {
            $('#update').modal();
            return false;
        });
        $('.addMe').click(function () {
            $('#showMe').modal();
            return false;
        });
        function OnSuccess() {
            $("#mainList").hide();
            $("#show").show();
        }
</script>
