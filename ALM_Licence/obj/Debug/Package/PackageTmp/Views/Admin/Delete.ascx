﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ALM_Licence.Models.Licence>" %>

 <% if (ViewData["Warning"] != null)
    { %>
    <fieldset id="licenceInfo1">
                <legend></legend>
        <div>
        [Serial Number :<strong> Cannot Be Found </strong>]
        <br /><br /><br /><br />
        <hr />
            <div style = "color:red">
                <%: ViewData["Warning"]%>
            </div>
        <hr />
        <br /><br />
        </div>
     </fieldset>
<% }%>
<% else { %>
               
    <h2 class = "legend">Do you really want to remove this [Serial Number : <%: Model.LicenceNumber %>]?</h2>
  

            <fieldset id="licenceInfo">
                <legend></legend>
            
               <div class="editor-label">
                   First Name
                </div>
                <div class="editor-field">
                    <%: Html.DisplayFor(model => model.FirstName) %>
                </div><br />
              <div class="editor-label">
                    Last Name
                </div>
                <div class="editor-field">
                    <%: Html.DisplayFor(model => model.LastName)%>
                </div><br />
                <div class="editor-label">
                    Validity Start
                </div>
                <div class="editor-field">
                    <% if(Model.Start != null){ %>
                    <%: Model.Start.Value.ToShortDateString()%>
                    <% } %>
                </div>
                <br />
                <div class="editor-label">
                  Validity End
                </div>
                <div class="editor-field">
                    <% if (Model.End != null){ %>
                    <%: Model.End.Value.ToShortDateString()%>
                    <% } %>
                </div>
                <br />
                <div class="editor-label">
                    Bank ALM
                </div>
                <div class="editor-field">
                    <% if(Model.BankALM == true){ %>
                   <input type="checkbox" disabled="disabled" checked="checked"/>
                    <% }%>
                    <% else { %>
                    <input type="checkbox" disabled="disabled" />
                    <% } %>
                </div>
                <br />
                <div class="editor-label">
                   Life Insurance
                </div>
                <div class="editor-field">
                    <% if (Model.LifeInsurance == true){ %>
                   <input type="checkbox" disabled="disabled" checked="checked"/>
                    <% }%>
                    <% else { %>
                    <input type="checkbox" disabled="disabled" />
                    <% } %>
                </div>
                <br />
                <div class="editor-label">
                    Pension Fund
                </div>
                <div class="editor-field">
                    <% if (Model.PensionFund == true) { %>
                   <input type="checkbox" disabled="disabled" checked="checked"/>
                    <% }%>
                    <% else { %>
                    <input type="checkbox" disabled="disabled" />
                    <% } %>
                </div>
                <br />
                 <div class="editor-label">
                   Porfolio Analysis
                </div>
                <div class="editor-field">
                   <% if (Model.PortfolioAnalysis == true)  { %>
                   <input type="checkbox" disabled="disabled" checked="checked"/>
                    <% }%>
                    <% else { %>
                    <input type="checkbox" disabled="disabled" />
                    <% } %>
                </div>
                <br />
                <div class="editor-label">
                   Islamic Finance
                </div>
                <div class="editor-field">
                    <% if (Model.IslamicFinance == true)  { %>
                   <input type="checkbox" disabled="disabled" checked="checked"/>
                    <% }%>
                    <% else { %>
                    <input type="checkbox" disabled="disabled" />
                    <% } %>
                </div>
            <br />
        <div class="buttons">      
        <div class="buttons" style="float:left"> <% using (Html.BeginForm()) {%>
            <%: Html.ValidationSummary(true) %>
        
                  &nbsp;<input type="submit" value="Confirm" />
                   <%: Html.Hidden("id", (int)Model.Licence_ID) %>
                   <%: Html.Hidden("confirm", (int)1) %>
          

        <% } %>
        </div>
        <div class="buttons" style="float:right">
         <% using (Html.BeginForm()) {%>
            <%: Html.ValidationSummary(true) %>
             <div class="buttons">
                  &nbsp;<input type="submit" value="Cancel" />
                   <%: Html.Hidden("id", (int)Model.Licence_ID) %>
                   <%: Html.Hidden("cancel", (int)0) %>
                </div>

        <% } %>
        </div>
        </div>

         </fieldset>

<% } %>

