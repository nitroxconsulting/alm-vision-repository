﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ALM_Licence.Models.Licence>" %>

    

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Admin
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script src="../../Scripts/jquery-1.10.2.js" type="text/javascript"></script>
<script src="../../Scripts/jquery-ui.js" type="text/javascript"></script>

<div class="menucontainer">
    <ul id="menu">              
        <li><%: Html.ActionLink("Add Licences", "Create", "Admin")%></li>
        <li><%: Html.ActionLink("Show Licences", "List", "Admin")%></li>
    </ul>
</div>
<br /><br />

<% if (ViewData["Warning"] != null)
    { %>
     <div>
        <fieldset id="licenceInfo1">
                    <legend></legend>
       
            Warning : Cannot Update Licence
            <br /><br /><br /><br />
            <hr />
                <div style = "color:red">
                    <%: ViewData["Warning"]%>
                </div>
            <hr />
            <br /><br />
            <br />
               <strong> <%: Html.ActionLink("Go To Licence", "Check_Data", new { id = ViewData["id"] })%></strong>
            <br />
         </fieldset>
     </div>
<% }%>
<% else { %>

<div id = "update">
  <% using (Html.BeginForm("Update","Admin",FormMethod.Post)) {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset id="licenceOption">
            <legend>Update Information </legend>
            [Serial Number :<strong> <%: Model.LicenceNumber %> </strong>]
            <br /><br />
            <div id = "grid1">
               <div class="editor-label">
                   First Name
                </div>
                <div class="editor-field-j">
                    <%: Html.TextBoxFor(model => model.FirstName) %>
                    <%: Html.ValidationMessageFor(model => model.FirstName) %>
                </div><br />
              <div class="editor-label">
                    Last Name
                </div>
                <div class="editor-field-j">
                    <%: Html.TextBoxFor(model => model.LastName) %>
                    <%: Html.ValidationMessageFor(model => model.LastName) %>
                </div><br />
                <div class="editor-label">
                    Validity Start
                </div>
                <div class="editor-field-j">
                <% if (Model.Start != null){%>
                    <%: Html.TextBox("Start",Model.Start.Value.ToShortDateString())%>
                    <%: Html.ValidationMessageFor(model => model.Start)%>
                 <% } %>
                  <% else {%>
                    <%: Html.TextBox("Start", Model.Start)%>
                    <%: Html.ValidationMessageFor(model => model.Start)%>
                
                <% } %>
           
                 <script type="text/javascript">
                     $(document).ready(function () {
                         $('#Start').datepicker({
                             changeMonth: true,
                             changeYear: true,
                             dateFormat: 'mm/dd/yy'
                         });
                     }); 
                    </script>
                </div>
                <br />
                <div class="editor-label">
                  Validity End
                </div>
                <div class="editor-field-j">
                <% if (Model.End != null){%>
                    <%: Html.TextBox("End", Model.End.Value.ToShortDateString())%>
                 
                 <% } %>
                 <% else{%>
                    <%: Html.TextBox("End", Model.End)%>
                 <% } %>
                <%: Html.ValidationMessageFor(model => model.End)%>
                 <script type="text/javascript">
                     $(document).ready(function () {
                         $('#End').datepicker({
                             changeMonth: true,
                             changeYear: true,
                             dateFormat: 'mm/dd/yy'
                         });
                     }); 
                    </script>
                </div>
                <br />
                <%: Html.HiddenFor(model => model.Licence_ID) %>
                <%: Html.HiddenFor(model => model.LicenceNumber) %>
                <div class="editor-label">
                    Bank ALM
                </div>
                <div class="editor-field-j">
                    <%: Html.CheckBox("BankALM")%>
                    <%: Html.ValidationMessageFor(model => model.BankALM) %>
                </div>
                <br />
                <div class="editor-label">
                   Life Insurance
                </div>
                <div class="editor-field-j">
                    <%: Html.CheckBox("LifeInsurance")%>
                    <%: Html.ValidationMessageFor(model => model.LifeInsurance) %>
                </div>
                <br />
                <div class="editor-label">
                    Pension Fund
                </div>
                <div class="editor-field-j">
                    <%: Html.CheckBox("PensionFund")%>
                    <%: Html.ValidationMessageFor(model => model.PensionFund) %>
                </div>
                <br />
                 <div class="editor-label">
                   Porfolio Analysis
                </div>
                <div class="editor-field-j">
                    <%: Html.CheckBox("PortfolioAnalysis")%>
                    <%: Html.ValidationMessageFor(model => model.PortfolioAnalysis) %>
                </div>
                <br />
                <div class="editor-label">
                    Islamic Finance
                </div>
                <div class="editor-field-j">
                    <%: Html.CheckBox("IslamicFinance")%>
                    <%: Html.ValidationMessageFor(model => model.IslamicFinance) %>
                </div>
                <br />
           
                <div class="buttons">
                  &nbsp;<input type="submit" value="Update" />
                </div>
            </div>
             <div>
                <div class="divOptions">Check User Type :  <hr /><hr /></div>
                <br />
                 <div id = "grid2">
                    <div class="editor-label">
                     &nbsp;&nbsp;&nbsp;    Client
                    </div>
                   <div class="editor-field-j">
                        <% if (Model.Option_1 == true){%>
                            <%: Html.RadioButton("User","Option_1",isChecked : true)%>
                        <% } %>
                        <% else{%>
                            <%: Html.RadioButton("User", "Option_1")%>
                         <% } %>
                        <%: Html.HiddenFor(model => model.Option_1)%>
                    </div>
                    <hr />
                    <div class="editor-label">
                      &nbsp;&nbsp;&nbsp;    Admin
                    </div>
                    <div class="editor-field-j">
                         <% if (Model.Option_2 == true){%>
                            <%: Html.RadioButton("User","Option_2",isChecked : true)%>
                         <% } %>
                         <% else{%>
                            <%: Html.RadioButton("User", "Option_2")%>
                         <% } %>
                         <%: Html.HiddenFor(model => model.Option_2)%>
                    </div>
                    <hr />
                    <div class="editor-label">
                    &nbsp;&nbsp;&nbsp;   Developer
                    </div>
                    <div class="editor-field-j">
                         <% if (Model.Option_3 == true){%>
                            <%: Html.RadioButton("User","Option_3",isChecked : true)%>
                         <% } %>
                         <% else{%>
                            <%: Html.RadioButton("User", "Option_3")%>
                         <% } %>
                         <%: Html.HiddenFor(model => model.Option_3)%>
                    </div>
                     <%--<hr />
                     <div class="editor-label">
                     &nbsp;&nbsp;&nbsp;    Option 4
                    </div>
                    <div class="editor-field-j">
                        <%: Html.CheckBox("LifeInsurance")%>
                        <%: Html.ValidationMessageFor(model => model.LifeInsurance) %>
                    </div>
                    <hr />
                    <div class="editor-label">
                     &nbsp;&nbsp;&nbsp;     Option 5
                    </div>
                    <div class="editor-field-j">
                        <%: Html.CheckBox("PensionFund")%>
                        <%: Html.ValidationMessageFor(model => model.PensionFund) %>
                    </div>
                    <hr />
                    <div class="editor-label">
                     &nbsp;&nbsp;&nbsp;  Option 6
                    </div>
                    <div class="editor-field-j">
                        <%: Html.CheckBox("PortfolioAnalysis")%>
                        <%: Html.ValidationMessageFor(model => model.PortfolioAnalysis) %>
                    </div>
                </div>
                <div id = "grid3">
                    <div class="editor-label">
                     | &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;    Option 7
                    </div>
                    <div class="editor-field-j">
                        <%: Html.CheckBox("LifeInsurance")%>
                        <%: Html.ValidationMessageFor(model => model.LifeInsurance) %>
                    </div>
                    <hr />
                    <div class="editor-label">
                     | &nbsp;&nbsp;&nbsp;   &nbsp;&nbsp;&nbsp;    Option 8
                    </div>
                    <div class="editor-field-j">
                        <%: Html.CheckBox("PensionFund")%>
                        <%: Html.ValidationMessageFor(model => model.PensionFund) %>
                    </div>
                    <hr />
                    <div class="editor-label">
                     | &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;   Option 9
                    </div>
                    <div class="editor-field-j">
                        <%: Html.CheckBox("PortfolioAnalysis")%>
                        <%: Html.ValidationMessageFor(model => model.PortfolioAnalysis) %>
                    </div>
                     <hr />
                     <div class="editor-label">
                     | &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;    Option 10
                    </div>
                    <div class="editor-field-j">
                        <%: Html.CheckBox("LifeInsurance")%>
                        <%: Html.ValidationMessageFor(model => model.LifeInsurance) %>
                    </div>
                    <hr />
                    <div class="editor-label">
                     | &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;    Option 11
                    </div>
                    <div class="editor-field-j">
                        <%: Html.CheckBox("PensionFund")%>
                        <%: Html.ValidationMessageFor(model => model.PensionFund) %>
                    </div>
                    <hr />
                    <div class="editor-label">
                     | &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  Option 12
                    </div>
                    <div class="editor-field-j">
                        <%: Html.CheckBox("PortfolioAnalysis")%>
                        <%: Html.ValidationMessageFor(model => model.PortfolioAnalysis) %>
                    </div>--%>
                </div>
            </div>
        </fieldset>

    <% } %>
</div>
<% } %>
</asp:Content>



