﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ALM_Licence.Models.ALMEntities>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Activate
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <fieldset>
        <legend>ALM Solution License Activation</legend>
        <div>
        <table class="activate">
        <tr>
            <td> Licence number :</td><td> <%: Html.TextBox("Serial_Nmber", ViewData["param1"], new { disabled = "disabled" })%> </td>
        </tr>
        <tr>
            <td>Activation Key  :</td><td><textarea rows="5" cols="130" readonly> <%:ViewData["param10"]%> </textarea></td>
            <script type="text/javascript">
                $('textarea').on('mouseup', function () { $(this)[0].select(); });
            </script>
        </tr>
      </table>
       </div>
    </fieldset>
</asp:Content>
