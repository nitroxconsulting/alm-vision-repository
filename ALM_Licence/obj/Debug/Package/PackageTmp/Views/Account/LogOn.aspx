﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ALM_Licence.Models.LogOnModel>" %>

<asp:Content ID="loginTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Log On
</asp:Content>

<asp:Content ID="loginContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Log On</h2>
    <p>
        Please enter your username and password. <%: Html.ActionLink("Register", "Register") %> if you don't have an account.
    </p>

   <% using (Html.BeginForm("LogIn_Credentials", "LogIn", FormMethod.Post))
      {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset id="licenceInfo">
            <legend>Licence Data - Information</legend>
            
           <div class="editor-label">
              User Name
            </div>
            <div class="editor-field-j">
                <%: Html.TextBox("username") %>
            </div><hr />
            <div class="editor-label">
               Password
            </div>
            <div class="editor-field-j">
                <%: Html.Password("password") %>
            </div>
            <hr />
        </fieldset>
       
    <% } %>
</asp:Content>
