﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<script type="text/javascript">
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
             m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-64197541-1', 'auto');
    ga('send', 'pageview');
  </script>

<div style="background-color:black;color:white;position:absolute;top: 0px;bottom: 0px; width:100%">

    <div style="width:100%;height:50%">
        <div style="float:left;width:50%"><img alt="" src="../../Images/under-construction.PNG" /></div>
        <br />
        <div style="float:right;text-align:center;font-size:medium;width:50%;height:50%"><br /><br /><br /><br /><br /><br /><strong>Still a Parking Page, Sorry for the Inconvenience.....</strong></div>
    </div>
    <br />
    <hr /><div style="text-align:center;bottom:10px;font-size:medium"><br />Thanks!<br /> Copyright @ <%: System.DateTime.Now.Year %></div>
</div>