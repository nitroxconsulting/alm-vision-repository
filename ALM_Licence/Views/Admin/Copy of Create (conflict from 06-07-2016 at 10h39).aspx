﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ALM_Licence.Models.Licence>" %>

    

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Admin
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script src="../../Scripts/jquery-1.10.2.js" type="text/javascript"></script>
<script src="../../Scripts/jquery-ui.js" type="text/javascript"></script>

<div class="menucontainer">
    <ul id="menu">              
        <li><%: Html.ActionLink("Show Licences", "List", "Admin")%></li>
    </ul>
</div>
<br /><br />
    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset id="licenceOption">
            <legend>Licence Data - Information</legend>
             <br/>
             <div id = "grid1">
               <div class="editor-label">
                   First Name
                </div>
                <div class="editor-field-j">
                    <%: Html.TextBoxFor(model => model.FirstName) %>
                    <%: Html.ValidationMessageFor(model => model.FirstName) %>
                </div><hr />
              <div class="editor-label">
                    Last Name
                </div>
                <div class="editor-field-j">
                    <%: Html.TextBoxFor(model => model.LastName) %>
                    <%: Html.ValidationMessageFor(model => model.LastName) %>
                </div><hr />
                <div class="editor-label">
                    Validity Start
                </div>
                <div class="editor-field-j">
                    <%: Html.TextBoxFor(model => model.Start)%>
                    <%: Html.ValidationMessageFor(model => model.Start) %>
                     <script type="text/javascript">
                         $(document).ready(function () {
                              var dateToday = new Date();
                              var newDate = dateToday.getFullYear() + "-01-01";
                              $('#Start').datepicker({ dateFormat: 'yy-mm-dd' }).datepicker('setDate', newDate); ;
                        }); 
                    </script>
                </div>
                <hr />
                <div class="editor-label">
                  Validity End
                </div>
                <div class="editor-field-j">
                    <%: Html.TextBoxFor(model => model.End) %>
                     <%: Html.ValidationMessageFor(model => model.End) %>
                    <script type="text/javascript">
                        var dateToday = new Date();
                        var getNewYear = (dateToday.getFullYear() + 1);
                        var newDate = getNewYear + "/" + (dateToday.getMonth() + 1) + "/" + dateToday.getDate();
                         $(document).ready(function () {
                             $('#End').datepicker({
                                 changeMonth: true,
                                 changeYear: true,
                                 dateFormat: 'yy-mm-dd',
                                 minDate: 'today' 
                             });
                             $('#End').datepicker('setDate', new Date(Date.parse(newDate)));

                         });
                  
                    </script>
                </div>
                <hr/>
                <div class="editor-label">
                    Bank ALM
                </div>
                <div class="editor-field-j">
                    <%: Html.CheckBox("BankALM", new { @checked = "checked" })%>
                    <%: Html.ValidationMessageFor(model => model.BankALM) %>
                </div>
                <hr />
                <div class="editor-label">
                   Life Insurance
                </div>
                <div class="editor-field-j">
                    <%: Html.CheckBox("LifeInsurance", new { @checked = "checked" })%>
                    <%: Html.ValidationMessageFor(model => model.LifeInsurance) %>
                </div>
                <hr />
                <div class="editor-label">
                    Pension Fund
                </div>
                <div class="editor-field-j">
                    <%: Html.CheckBox("PensionFund",  new { @checked = "checked" })%>
                    <%: Html.ValidationMessageFor(model => model.PensionFund) %>
                </div>
                <hr />
                <div class="editor-label">
                   Porfolio Analysis
                </div>
                <div class="editor-field-j">
                    <%: Html.CheckBox("PortfolioAnalysis", new { @checked = "checked" })%>
                    <%: Html.ValidationMessageFor(model => model.PortfolioAnalysis) %>
                </div>
                  <hr />
                 <div class="editor-label">
                   Islamic Finance
                </div>
                <div class="editor-field-j">
                    <%: Html.CheckBox("IslamicFinance")%>
                    <%: Html.ValidationMessageFor(model => model.IslamicFinance) %>
                </div>
               <hr /><br />
                <div class="buttons">
                  &nbsp;<input type="submit" value="Save" />
                </div>
              </div>
              <div>
                <div class="divOptions">Check User Type :  <hr /><hr /></div>
                <br />
                 <div id = "grid2">
                    <div class="editor-label">
                     &nbsp;&nbsp;&nbsp;    Client
                    </div>
                    <div class="editor-field-j">
                        <%: Html.RadioButton("User","C")%>
                    </div>
                    <hr />
                    <div class="editor-label">
                      &nbsp;&nbsp;&nbsp;    Admin
                    </div>
                    <div class="editor-field-j">
                         <%: Html.RadioButton("User", "A")%>
                    </div>
                    <hr />
                    <div class="editor-label">
                    &nbsp;&nbsp;&nbsp;   Developer
                    </div>
                    <div class="editor-field-j">
                         <%: Html.RadioButton("User", "D")%>
                    </div>
                   <%--  <hr />
                     <div class="editor-label">
                     &nbsp;&nbsp;&nbsp;    Option 4
                    </div>
                    <div class="editor-field-j">
                        <%: Html.CheckBox("LifeInsurance")%>
                        <%: Html.ValidationMessageFor(model => model.LifeInsurance) %>
                    </div>
                    <hr />
                    <div class="editor-label">
                     &nbsp;&nbsp;&nbsp;     Option 5
                    </div>
                    <div class="editor-field-j">
                        <%: Html.CheckBox("PensionFund")%>
                        <%: Html.ValidationMessageFor(model => model.PensionFund) %>
                    </div>
                    <hr />
                    <div class="editor-label">
                     &nbsp;&nbsp;&nbsp;  Option 6
                    </div>
                    <div class="editor-field-j">
                        <%: Html.CheckBox("PortfolioAnalysis")%>
                        <%: Html.ValidationMessageFor(model => model.PortfolioAnalysis) %>
                    </div>
                </div>
                <div id = "grid3">
                    <div class="editor-label">
                     | &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;    Option 7
                    </div>
                    <div class="editor-field-j">
                        <%: Html.CheckBox("LifeInsurance")%>
                        <%: Html.ValidationMessageFor(model => model.LifeInsurance) %>
                    </div>
                    <hr />
                    <div class="editor-label">
                     | &nbsp;&nbsp;&nbsp;   &nbsp;&nbsp;&nbsp;    Option 8
                    </div>
                    <div class="editor-field-j">
                        <%: Html.CheckBox("PensionFund")%>
                        <%: Html.ValidationMessageFor(model => model.PensionFund) %>
                    </div>
                    <hr />
                    <div class="editor-label">
                     | &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;   Option 9
                    </div>
                    <div class="editor-field-j">
                        <%: Html.CheckBox("PortfolioAnalysis")%>
                        <%: Html.ValidationMessageFor(model => model.PortfolioAnalysis) %>
                    </div>
                     <hr />
                     <div class="editor-label">
                     | &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;    Option 10
                    </div>
                    <div class="editor-field-j">
                        <%: Html.CheckBox("LifeInsurance")%>
                        <%: Html.ValidationMessageFor(model => model.LifeInsurance) %>
                    </div>
                    <hr />
                    <div class="editor-label">
                     | &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;    Option 11
                    </div>
                    <div class="editor-field-j">
                        <%: Html.CheckBox("PensionFund")%>
                        <%: Html.ValidationMessageFor(model => model.PensionFund) %>
                    </div>
                    <hr />
                    <div class="editor-label">
                     | &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  Option 12
                    </div>
                    <div class="editor-field-j">
                        <%: Html.CheckBox("PortfolioAnalysis")%>
                        <%: Html.ValidationMessageFor(model => model.PortfolioAnalysis) %>
                    </div>--%>
                </div>
             </div>
        </fieldset>

    <% } %>

</asp:Content>

