﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	MSI_List
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <center>
    <div id = "displayMSI">

        <div id="latestVer"><strong> Latest Version </strong> </div>
        <br /><hr /><br />
            <% if (ViewData["existing"] == null)
                { %>
                    <div>
                       <span style="color:#FFFACD"><strong> For Beta &nbsp;&nbsp;:&nbsp;&nbsp;</strong></span> <span id="MSI_Version1"> <%:ViewData["for_Beta"]%> </span>
                        <br />
                       <span style="color:#FFFACD"><strong> For Non - Beta &nbsp;&nbsp;:&nbsp;&nbsp;</strong></span> <span id="MSI_Version2"><%:ViewData["for_nonBeta"]%> </span>
                    </div>
            <% } %>
            <% else { %>
                <span style="color:aqua"><strong> <%:ViewData["existing"]%></strong></span>
            <% } %>
    </div>
    </center>
</asp:Content>
