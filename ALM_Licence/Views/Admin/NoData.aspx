﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	NoData
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <fieldset id="licenceInfo">
        <legend>Update Information </legend>
        [Serial Number :<strong> Cannot Be Found </strong>]
        <br /><br /><br /><br />
        <hr />
            <div style = "color:red">
               <%: ViewData["Warning"]%>
            </div>
        <hr />
        <br /><br />
    </fieldset>

</asp:Content>
