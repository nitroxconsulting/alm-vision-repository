﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<ALM_Licence.Models.LicenceMeta_vw>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Search
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script src="../../Scripts/MicrosoftAjax.debug.js" type="text/javascript"></script>
<script src="../../Scripts/MicrosoftMvcAjax.debug.js" type="text/javascript"></script>
    
<div class="searchcontainer">
    <% using (Html.BeginForm("Item", "Search",FormMethod.Post))
       { %>
              <%: Html.TextBox("search")%>
               &nbsp;<input type="image" src="../../Images/blue-search-icon.png" alt="search"/>
      
       
      <% } %>
</div>
<div class="menucontainer">
    <ul id="menu">              
        <li><%: Html.ActionLink("Add Licence", "Create", "Admin")%></li>          
        <li><%: Html.ActionLink("Show Licences", "List", "Admin")%></li>
    </ul>
</div>
<br /><br /><br /><br />

<div id = "searchDiv">
</div>
<% if (ViewData["none"] != null) {%>
    <span style="color:red"><%: ViewData["none"] %></span>
<% }%>
<% else if (ViewData["none"] == null)
   {%>
  <div style="width:2200px;height:700px">
    <table class="list" id="mainList">
        <tr>
        <th style="width:100px;"> Action</th>
            <th style="width:200px">
                Serial Number
            </th>
            <th style="width:250px">
                First Name
            </th>
            <th style="width:250px">
               Last Name
            </th>
            <th style="width:180px">
               Company
            </th>
            <th style="width:150px">
               Phone
            </th>
             <th style="width:240px">
               Mail
            </th>
             <th style="width:100px">
               Validity<br /> Start
            </th>
             <th style="width:100px">
               Validity<br /> End
            </th>
            <th style="width:80px">
              <span class="bool-size"> Bank <br />ALM</span>
            </th>
            <th style="width:80px">
              <span class="bool-size"> Life <br />Insurance</span>
            </th>
            <th style="width:80px">
              <span class="bool-size"> Pension<br /> Fund</span>
            </th>
            <th style="width:80px">
              <span class="bool-size">Portfolio <br />Analysis</span>
            </th>
            <th style="width:80px">
               <span class="bool-size">Sensitivity <br />Analysis</span>
            </th>
            <th style="width:100px">
              Last <br />Downloaded <br />Version
            </th>
            <th style="width:280px">
              Last <br />Downloaded <br />Date
            </th>
             <th style="width:250px">
              Licence <br />Activation <br />Date
            </th>
        </tr>
    <% foreach (var item in Model) { %>
    <%--<% int myId = 0; %>--%>
    
          <tr>
            <td>
             <div style="width:110px">
                <span class="editMe">
                <%: Html.ActionLink("Update", "Check_Data", new { id = item.Licence_ID })%>
                    <%--<%: Ajax.ActionLink("Update", "Check_Data", "Admin", new { id = item.Licence_ID },
                    new AjaxOptions
                    {
                        UpdateTargetId = "update",
                        InsertionMode = InsertionMode.Replace,
                        HttpMethod = "GET"
                    },new {@class="updateMe"})%> --%>

                </span> 
                <span class="deleteMe">
                    <%: Ajax.ActionLink("Delete", "Delete", "Admin", new { id = item.Licence_ID },
                    new AjaxOptions
                    {
                        UpdateTargetId = "update",
                        InsertionMode = InsertionMode.Replace,
                        HttpMethod = "GET"
                    },new {@class="deleteMe"})%>
                </span>
            </div>
            </td>
             <td align="center">
                <%: item.LicenceNumber %>
            </td>
           <td align="justify" >
                <%: item.FirstName %>
            </td>
            <td align="justify">
                <%: item.LastName %>
            </td>
             <td align="justify">
                <%: item.metadata_company %>
            </td>
            <td align="left">
                <%: item.metadata_phone %>
            </td>
            <td align="justify">
                <%: item.metadata_mail %>
            </td>
            <td align="center">
                <% if(item.Start != null){ %>
                <%: item.Start.Value.ToShortDateString() %>
                <% } %>
            </td>
            <td align="center">
                <% if (item.End != null){ %>
               <%: item.End.Value.ToShortDateString() %>
               <% } %>
            </td>
            <td>
                <% if(item.BankALM == true){ %>
               <input type="checkbox" disabled="disabled" checked="checked"/>
                <% }%>
                <% else { %>
                <input type="checkbox" disabled="disabled" />
                <% } %>
            </td>
            <td>
                <% if(item.LifeInsurance == true){ %>
                 <input type="checkbox" disabled="disabled" checked="checked"/>
                <% }%>
                <% else { %>
                <input type="checkbox" disabled="disabled" />
                <% } %>
            </td>
            <td>
                <% if(item.PensionFund == true){ %>
                <input type="checkbox" disabled="disabled" checked="checked"/>
                <% }%>
                <% else { %>
                <input type="checkbox" disabled="disabled" />
                <% } %>
            </td>
            <td>
                <% if(item.PortfolioAnalysis == true){ %>
                <input type="checkbox" disabled="disabled" checked="checked"/>
                <% }%>
                <% else { %>
               <input type="checkbox" disabled="disabled" />
                <% } %>
            </td>
             <td>
                <% if(item.IslamicFinance == true){ %>
                <input type="checkbox" disabled="disabled" checked="checked"/>
                <% }%>
                <% else { %>
                <input type="checkbox" disabled="disabled" />
                <% } %>
            </td>
              <td>
                <%: item.last_Downloaded_Version%>
            </td>
            <td>
                <%: item.last_Downloaded_Date%>
            </td>
            <td>
                <%: item.date_Activated%>
            </td>
        </tr>
    
    <% } %>

    </table>
    </div>
<% } %>

    <script type ="text/javascript">
        $('.updateMe1').click(function () {
            $('#update1').modal();
            return false;
        });
        $('.deleteMe1').click(function () {
            $('#update1').modal();
            return false;
        });
        $('.addMe').click(function () {
            $('#showMe').modal();
            return false;
        });
        function OnSuccess() {
            $("#mainList").hide();
            $("#show").show();
        }
</script>

<div id = "update1">  </div>
</asp:Content>
