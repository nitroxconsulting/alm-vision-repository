﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<script type="text/javascript">
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-64197541-1', 'auto');
    ga('send', 'pageview');
</script>

<head>
    <title>
      Latest Version
    </title>
</head>
<div style="color:white;position:absolute;top: 0px;bottom: 0px; width:100%">

   <center>
    <div style="display: inline-block;width:100%;max-height:540;margin-top:40px">
        <img src="../../Images/banner1.PNG"alt="download" />
        <div style="width:50%;margin-top:-60px;">
          <% if (ViewData["param3"] != null) {%>
                 <% using (Html.BeginForm("Download", "Update",FormMethod.Get))
                   { %>
               
                     <%: Html.Hidden("filename", (string)ViewData["param2"]) %>
                     &nbsp;<input type="image" src="../../Images/uploadNow.PNG" alt="download"/><br />
                     <div style="color:black"> <strong><%: ViewData["filename"] %></strong></div>
                 <% } %>
           <% }%>
    
        <br />
      </div>
       
   </div><br />
  <div style="color:#333"><strong><%: ViewData["param1"] %> </strong><br /> <strong><%: ViewData["param2"] %> </strong> </div>
 </center>
</div>