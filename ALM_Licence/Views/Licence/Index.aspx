﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Home Page
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
             m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-64197541-1', 'auto');
    ga('send', 'pageview');
  </script>

   <% using (Html.BeginForm("LogIn_Credentials", "LogIn",FormMethod.Post)) {%>
        <%: Html.ValidationSummary(true) %>

        <fieldset id="licenceInfo">
            <legend>Licence Data - Information</legend>
            
           <div class="editor-label">
               User Name
            </div>
            <div class="editor-field-j">
                <%: Html.TextBox("username") %>
            </div><hr />
            <div class="editor-label">
               Password
            </div>
            <div class="editor-field-j">
                <%: Html.Password("password") %>
            </div>
            <hr />
            <div>
                <% if(ViewData["error"] != null){ %>
                
                  <span class="error">  <%: ViewData["error"] %>  </span>
                <% }  %>
            </div>
            <br />
            <div class="buttons">
              &nbsp;<input type="submit" value="LOG IN" />
            </div>
        </fieldset>
    <% } %>

</asp:Content>
