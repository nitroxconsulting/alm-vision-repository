﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using ALM_Licence.Controllers;

namespace ALM_Licence.Controllers
{

    [HandleError]
    public class HomeController : Controller
    {
        private static string folder = @"C:\WEB_Sites\ALM-vision.com\Downloads";

        //Landing Controller action that will render its landing page base on the url used by the user.
        public ActionResult Index()
        {
           
            setSecureProtocol(false);
            return RedirectToAction("Home", "Site");
        }

        //filename has no extension yet
        //need to get the exact filename with extension from the directory
        [HttpGet]
        public FileResult Download(String filename)
        {

            var checkFileName = Directory.GetFiles(folder).Where(fi => fi.Contains(filename)).FirstOrDefault();
            string fname = Path.GetFileName(checkFileName);
            return File(checkFileName, "application/msi", fname);
        }

       
        public ActionResult About()
        {
            return View();
        }

        //automatically set the protocol
        public static void setSecureProtocol(bool bSecure)
        {
            
            string redirectUrl = null;
            System.Web.HttpContext context = System.Web.HttpContext.Current;

    
            //if we want HTTPS and it is currently HTTP
            if (bSecure && !context.Request.IsSecureConnection){
                redirectUrl = context.Request.Url.ToString().Replace("http:", "https:");
            }
            else if (!bSecure && context.Request.IsSecureConnection)
            {
                if (!bSecure && context.Request.IsSecureConnection)
                    redirectUrl = context.Request.Url.ToString().Replace("https:", "http:");
            }
            //else

            // in all other cases we don't need to redirect

            // check if we need to redirect, and if so use redirectUrl to do the job
            if (redirectUrl != null)
               context.Response.Redirect(redirectUrl);

        }
    }
}
