﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ALM_Licence.Models;

namespace ALM_Licence.Controllers
{
    public class SearchController : Controller
    {
        //
        //db connection/
        private ALMEntities _db = new ALMEntities();

        public ActionResult Item(string search)
        {
            var contains = _db.LicenceMeta_vw.Where(
                x => x.FirstName.Contains(search) ||
                     x.LastName.Contains(search) ||
                     x.LicenceNumber.Contains(search) ||
                     x.metadata_company.Contains(search) ||
                     x.metadata_mail.Contains(search)).ToList();

            if(search == "")
            {
               ViewData["none"] = "Please specify your search";
               //return PartialView("NoContent");
                //return Content("<script language='javascript' type='text/javascript'>alert('Please specify your search');</script>");
            }
            else if (contains.Count == 0) 
            {
               ViewData["none"] = search + " cannot be found";
               //return PartialView("NoContent");
               
            }

            return View("Search", contains);
        }

    }
}
