﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ALM_Licence.Controllers
{
    public class SiteController : Controller
    {
        //
        // GET: /Site/

        public ActionResult Home()
        {
            return new FilePathResult(Server.MapPath("/site/") + "index.html", "text/html");
           // return Redirect("http://alm-vision.com");

        }

     }
}
