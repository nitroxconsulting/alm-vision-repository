﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ALM_Licence.Controllers
{
    public class DownloadsController : Controller
    {
        //
        // GET: /Downloads/

        public ActionResult Index()
        {
           
              var Update_Con = new UpdateController();
              string filename = Update_Con.getLatest("SetupALMSolutions.", 18);
              ViewData["filename"] = filename;

               return PartialView("Download");
            
        }

    }
}
