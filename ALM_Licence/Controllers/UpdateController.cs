﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ALM_Licence.Models;
using ALM_Licence.Controllers;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;

namespace ALM_Licence.Controllers
{
    public class UpdateController : Controller
    {
        //
        // GET: /Update/

        private ALMEntities _db = new ALMEntities();
        private static string folder = @"C:\WEB_Sites\ALM-vision.com\Downloads";
        

        public ActionResult Index()
        {
             string redirectUrl = null;
             string update = "://alm-vision.com/update";

             System.Web.HttpContext context = System.Web.HttpContext.Current;
             redirectUrl = context.Request.Url.ToString();

             if (redirectUrl.Contains(update))
             {

                    string first_Param = this.Request.QueryString["k"];
                    string version_Number = this.Request.QueryString["v"];
                    string licence_Key = this.Request.QueryString["lk"];
                    string isvalid_Licence = "false";
                    string setUp_type = "NonBeta";

                    if (this.Request.QueryString["l"] != null && this.Request.QueryString["l"] == "true")
                    {
                        isvalid_Licence = this.Request.QueryString["l"];
                    }

                    if (this.Request.QueryString["b"] != null && this.Request.QueryString["b"] == "true")
                    {
                        setUp_type = "Beta";
                    }
            
                    var Admin_Con = new AdminController();
                    string extract_Licence = Admin_Con.serial_Num(licence_Key);
                    var exists_Licence = _db.LicenceMeta_vw.Where(x => x.LicenceNumber == extract_Licence).FirstOrDefault();

                    ViewData["serial"] = extract_Licence;
                    if (exists_Licence == null)
                    {
                        ViewData["param1"] = "We do not recognise your licence, please use a valid licence!";
                        return View();
                    }

                    if (isvalid_Licence == "true")
                    {
                        ViewData["param1"] = "Your licence is no longer valid, please contact an ALM Vision representative!";
                        return View();
                    }

                    if (setUp_type == "Beta")
                    {
                        this.setType("SetupALMSolutionsBETA.",22,version_Number);
                    }
                    else if (setUp_type == "NonBeta")
                    {
                        this.setType("SetupALMSolutions.", 18, version_Number);
                    }

                return View();

              }
               ViewData["error"] = "Please Use correct address!";
            return RedirectToAction("Home", "Site");

        }
        
        [HttpGet]
        public FileResult Download(String filename)
        {
         return File(folder + "\\" + filename, "application/octet-stream", filename);
        }

        public void setType(string setUp_Type, int substring, string  version_Number) 
        {

            string filename = this.getLatest(setUp_Type,substring);
            string checkVersion = "";
            string getLastVersion = "";
            if (setUp_Type == "SetupALMSolutionsBETA."){
                checkVersion = version_Number;
                getLastVersion = filename.Substring(22);
            }
            else if (setUp_Type == "SetupALMSolutions.")
            {
                checkVersion = version_Number;
                getLastVersion = filename.Substring(18);
            }

            var version1 = new Version(checkVersion);
            var version2 = new Version(getLastVersion);
            var result = version1.CompareTo(version2);

            if (filename.Contains(version_Number))
            {
                string param1 = "You do have the latest version of ALM Solution ";
                if (setUp_Type == "SetupALMSolutionsBETA")
                {
                    param1 = param1 + "(Beta) ";
                }
                ViewData["param1"] = param1;
            }
            else if (result > 0)
            {
                string param1 = "You have a higher version of ALM Solution ";
                if (setUp_Type == "SetupALMSolutionsBETA")
                {
                    param1 = param1 + "(Beta) ";
                }
                param1 = param1 + ", Latest Version Available :";
                var checkFileName = Directory.GetFiles(folder).Where(fi => fi.Contains(filename)).ToList();
                string fname = Path.GetFileName(checkFileName[0]);
                ViewData["param1"] = param1;
                ViewData["param2"] = fname;
                ViewData["param3"] = "displayButton";
            }
            else
            {
                var checkFileName = Directory.GetFiles(folder).Where(fi => fi.Contains(filename)).ToList();
                string fname = Path.GetFileName(checkFileName[0]);
                ViewData["param1"] = "New Version Available :";
                ViewData["param2"] = fname;
                ViewData["param3"] = "displayButton";
                
            }
        }

        public string getLatest(string type, int substring)
        {
            var list = Directory.GetFiles(folder).Where(fi => fi.Contains(type)).OrderByDescending(fi => fi).ToList();
            string[] Beta = new string[list.Count];

            for (int i = 0; i < list.Count; i++)
            {
                string paths = list[i];
                string filenames = Path.GetFileName(paths);
                string ko = filenames.Substring(substring);
                string me = ko.Substring(0, ko.Length - 4);
                Beta[i] = me;
            }

            var sortedList = Beta.CustomSort().ToArray();
            string version = sortedList[sortedList.Length - 1];
            string path = type + version;
            string filename = Path.GetFileName(path);

            return filename;
        }
       
      } 
 }
public static class MyExtensions
{
    public static IEnumerable<string> CustomSort(this IEnumerable<string> list)
    {
        int maxLen = list.Select(s => s.Length).Max();

        return list.Select(s => new
        {
            OrgStr = s,
            SortStr = Regex.Replace(s, @"(\d+)|(\D+)", m => m.Value.PadLeft(maxLen, char.IsDigit(m.Value[0]) ? ' ' : '\xffff'))
        })
        .OrderBy(x => x.SortStr)
        .Select(x => x.OrgStr);
    }

}
