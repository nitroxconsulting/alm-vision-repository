﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ALM_Licence.Models;
using ALMSCommon;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;

namespace ALM_Licence.Controllers
{
    public class AdminController : Controller
    {
        //connection
        private ALMEntities _db = new ALMEntities();
        private static string folder = @"C:\WEB_Sites\ALM-vision.com\Downloads\";
        
        //
        // GET: /Admin/

        public ActionResult List()
        {
            string check_user = Convert.ToString(Session["user"]);
            string check_pass = Convert.ToString(Session["pass"]);

            if (LogInController.user == check_user && LogInController.pass == check_pass)
            {
                return View(_db.LicenceMeta_vw.ToList());
            }
            return RedirectToAction("Index","Home");
        }

        //
        // GET: /Admin/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Admin/Create
    [HttpGet]
        public ActionResult Create()
        {
            string check_user = Convert.ToString(Session["user"]);
            string check_pass = Convert.ToString(Session["pass"]);

            if (LogInController.user == check_user && LogInController.pass == check_pass)
            {
               // return PartialView("Create1");
                return View();
            }
            return RedirectToAction("Index", "Home");
        } 

        //
        // POST: /Admin/Create
       [HttpPost]
        public ActionResult Create([Bind(Exclude="Licence_ID")] Licence lic_entry, string User)
        {
                if (ModelState.IsValid)
                {
                    //lic_entry

                    lic_entry.Option_1 = false;
                    lic_entry.Option_2 = false;
                    lic_entry.Option_3 = false;

                    if(User == "Option_1"){
                        lic_entry.Option_1 = true;
                    }
                    else if (User == "Option_2")
                    {
                        lic_entry.Option_2 = true;
                    }
                    else if (User == "Option_3")
                    {
                        lic_entry.Option_3 = true;
                    }

                    lic_entry.LicenceNumber = this.get_serialNumber().ToUpper();
                    _db.Licences.AddObject(lic_entry);
                    _db.SaveChanges();
                    return RedirectToAction("List");
                   
                }
                else
                {
                    return View();
                }
            
           
        }
        
        //
        // GET: /Admin/Edit/5

        [HttpGet]
        public ActionResult Check_Data(int id)
        {
            var lic = _db.Licences.Where(x => x.Licence_ID ==  id).FirstOrDefault();
            if (lic != null)
            {
                string check_user = Convert.ToString(Session["user"]);
                string check_pass = Convert.ToString(Session["pass"]);

                if (LogInController.user == check_user && LogInController.pass == check_pass)
                {
                   return View("Update",lic); 
                }
            }
           
            ViewData["Warning"] = "Data has been removed already.";
            return View("NoData");
         }

        //
        // POST: /Admin/Edit/5

        [HttpPost]
        public ActionResult Update(Licence lic_entry,string User)
        {
            if (ModelState.IsValid)
            {
                lic_entry.Option_1 = false;
                lic_entry.Option_2 = false;
                lic_entry.Option_3 = false;

                if (User == "Option_1")
                {
                    lic_entry.Option_1 = true;
                }
                else if (User == "Option_2")
                {
                    lic_entry.Option_2 = true;
                }
                else if (User == "Option_3")
                {
                    lic_entry.Option_3 = true;
                }
                _db.Licences.Attach(_db.Licences.Single(x => x.Licence_ID == lic_entry.Licence_ID));
                _db.Licences.ApplyCurrentValues(lic_entry);
                _db.SaveChanges();
                return RedirectToAction("List");

            }
            else
            {
                ViewData["Warning"] = "Inputted Data is invalid.";
                ViewData["id"] = lic_entry.Licence_ID;
                return View();
            }
            
        }

        //
        // GET: /Admin/Delete/5
        [HttpGet]
        public ActionResult Delete(int id)
        {
            var lic = _db.Licences.Where(x => x.Licence_ID == id).FirstOrDefault();
            
            if (lic != null)
            {
                string check_user = Convert.ToString(Session["user"]);
                string check_pass = Convert.ToString(Session["pass"]);

                if (LogInController.user == check_user && LogInController.pass == check_pass)
                {
                    return PartialView(lic);
                }
            }

            ViewData["Warning"] = "Data has been removed already.";
            return PartialView(lic);
        }

        [HttpPost]
        public ActionResult Upload()
        {
            string isExisting = "";
            try
            {
                foreach (string inputTagName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[inputTagName];
                    string fileName =Path.GetFileName(file.FileName);
                    string filePath = Path.GetFullPath(folder + fileName);
                    string fileType = Path.GetExtension(file.FileName);

                    if (System.IO.File.Exists(filePath))
                    {
                        isExisting = "Solution Already Exists!";
                    }
                    else if (!fileName.Contains("SetupALMSolutions") && fileType != ".msi")
                    {
                        isExisting = "The Upload File is not an ALM Solution!";
                    }
                    else if (!System.IO.File.Exists(filePath) && file.ContentLength > 0 && fileType == ".msi" && fileName.Contains("SetupALMSolutions"))
                    {
                        file.SaveAs(filePath);
                    }
                 }
                return RedirectToAction("MSI_List", new { ifExist =  isExisting });
            }
            
            catch
            {
                return RedirectToAction("Index", "Home"); 
            }
        }

        public ActionResult MSI_List(string ifExist) 
        {
            ViewData["existing"] = ifExist;

            var Update_Con = new UpdateController();
            ViewData["for_Beta"] = Update_Con.getLatest("SetupALMSolutionsBETA.", 22);
            ViewData["for_nonBeta"] = Update_Con.getLatest("SetupALMSolutions.", 18);

            return View();
        }
        
        //
        // POST: /Admin/Delete/5

        [HttpPost]
        public ActionResult Delete(int id,int? confirm, int? cancel)
        {
            if(confirm != null)
            {
               try
               {
                   var tobeDeleted1 = _db.MetaDatas.Where(x => x.licence_id == id).FirstOrDefault();

                   if (tobeDeleted1 != null)
                   {
                       _db.MetaDatas.Attach(tobeDeleted1);
                       _db.MetaDatas.DeleteObject(tobeDeleted1);
                       _db.SaveChanges();

                   }

                    var toBeDeleted = _db.Licences.Where(x => x.Licence_ID == id).FirstOrDefault();
                    _db.Licences.Attach(toBeDeleted);
                    _db.Licences.DeleteObject(toBeDeleted);
                    _db.SaveChanges();

                    
                    return RedirectToAction("List");
               }
                catch
                {
                    return View();
                }
            }
            else if(cancel != null)
            {
                return RedirectToAction("List");
            }
            return RedirectToAction("List");
        }

        
        
        public string get_serialNumber()
        {
            Random random = new Random();
            Guid g;
            string uid;

            g = Guid.NewGuid();
            uid = g.ToString("D");
            uid = uid.Substring(4,19);

            return uid;
        }

        public string StripAccents(string s)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in s.Normalize(NormalizationForm.FormKD))
                switch (CharUnicodeInfo.GetUnicodeCategory(c))
                {
                    case UnicodeCategory.NonSpacingMark:
                    case UnicodeCategory.SpacingCombiningMark:
                    case UnicodeCategory.EnclosingMark:
                        break;

                    default:
                        sb.Append(c);
                        break;
                }
            return sb.ToString();
        }

        [HttpGet]
        public ActionResult Activate()
        {
            string redirectUrl = null;
            string licence = "licence.alm-vision.com";

            System.Web.HttpContext context = System.Web.HttpContext.Current;
            redirectUrl = context.Request.Url.ToString();

            if (redirectUrl.Contains(licence))
            {
             
                string serial_number = this.Request.QueryString["l"];
                string machine_Id = this.Request.QueryString["m"];
                string first_name = this.StripAccents(this.Request.QueryString["fn"]);
                string last_name = this.StripAccents(this.Request.QueryString["ln"]);
                string company = this.Request.QueryString["c"];
                string phone_number = this.Request.QueryString["p"];
                string email_address = this.Request.QueryString["e"];

                ViewData["param1"] = this.serial_Num(serial_number);
                ViewData["param2"] = machine_Id;
                ViewData["param3"] = first_name;
                ViewData["param4"] = last_name;
                ViewData["param5"] = company;
                ViewData["param6"] = phone_number;
                ViewData["param7"] = email_address;

                string sn = this.serial_Num(serial_number);
                var lic_info = _db.LicenceInfo_vw.Where(x => x.LicenceNumber == sn).Where(x => x.FirstName == first_name).Where(x => x.LastName == last_name).FirstOrDefault();

                if (lic_info == null)
                {
                    ViewData["param10"] = this.checkParam(sn);
                    return View();
                }

                int usage = this.check_MachineId(lic_info.Licence_ID, machine_Id);
                int exist = this.checkMetaData(lic_info.Licence_ID);

                if (exist == 0 && usage == 0)
                {
                    this.insertMetaData(machine_Id, lic_info.Licence_ID, lic_info.Modules, first_name, last_name, company, phone_number, email_address, lic_info.Option_1, lic_info.Option_2, lic_info.Option_3);

                    LicenceInfo target = new LicenceInfo();

                    target.LicenceNumber = serial_number;
                    target.MachineId = machine_Id;
                    target.Modules = lic_info.Modules;
                    target.FirstName = first_name;
                    target.LastName = last_name;
                    target.Company = company;
                    target.PhoneNumber = phone_number;
                    target.Email = email_address;
                    target.Start = Convert.ToDateTime(lic_info.Start);
                    target.End = Convert.ToDateTime(lic_info.End);
                    target.Option_1 = this.getOption(lic_info.Option_1);
                    target.Option_2 = this.getOption(lic_info.Option_2);
                    target.Option_3 = this.getOption(lic_info.Option_3);

                    string actual;
                    actual = target.Serialize();
                    //string test = li.Serialize();


                    ViewData["param8"] = this.deserializeHere(actual);
                    ViewData["param9"] = actual;


                    if (ViewData["param8"].Equals(ViewData["param9"]))
                    {
                        ViewData["param10"] = ViewData["param9"];
                    }
                    else
                    {
                        ViewData["param10"] = "not ok";
                    }
                }
                else if (exist == 1 && usage == 1)
                {

                    LicenceInfo target = new LicenceInfo();

                    target.LicenceNumber = serial_number;
                    target.MachineId = machine_Id;
                    target.Modules = lic_info.Modules;
                    target.FirstName = first_name;
                    target.LastName = last_name;
                    target.Company = company;
                    target.PhoneNumber = phone_number;
                    target.Email = email_address;
                    target.Start = Convert.ToDateTime(lic_info.Start);
                    target.End = Convert.ToDateTime(lic_info.End);
                    target.Option_1 = this.getOption(lic_info.Option_1);
                    target.Option_2 = this.getOption(lic_info.Option_2);
                    target.Option_3 = this.getOption(lic_info.Option_3);

                    string actual;
                    actual = target.Serialize();
                    //string test = li.Serialize();


                    ViewData["param8"] = this.deserializeHere(actual);
                    ViewData["param9"] = actual;


                    if (ViewData["param8"].Equals(ViewData["param9"]))
                    {
                        ViewData["param10"] = ViewData["param9"];
                    }
                    else
                    {
                        ViewData["param10"] = "not ok";
                    }
                }
                else if (usage == 0 && exist == 1)
                {
                    ViewData["param10"] = "LICENCE is ALREADY in USE";
                }
                    return View();
            }
            ViewData["error"] = "Please Use correct address!";
            return View("../Home/Index");
        }

        public string getOption(bool? optionValue) {

            if(optionValue == true){
                return "true";
            }

            return "false";
        }

        public string checkParam(string serial) {

           var lic_info = _db.LicenceInfo_vw.Where(x => x.LicenceNumber == serial).FirstOrDefault();
          
            if(lic_info != null){
                return "First/Last Name cannot be found. *Please check the characters of the data entered.";
            }

            return "Serial Number cannot be found";  
        }

        public string serial_Num(string serial_number) 
        {
            int countSerial = 0;
            int[] arr = new int[4];
            string newSerial = null;

            for (int i = 1; i <= serial_number.Length; i++)
            {
                if (i % 4 == 0)
                {
                    arr[countSerial] = i;
                    countSerial++;
                }
            }


            newSerial = serial_number.Insert(arr[0], "-").Insert(arr[1] + 1, "-").Insert(arr[2] + 2, "-");

            return newSerial;
        }

        public void insertMetaData(string machine_Id,int id,string serial,string fn, string ln,string company,string phone_number,string email_address, bool? op1, bool? op2, bool?  op3)
        {

            var checkMeta = _db.MetaDatas.Where(x => x.licence_id == id).FirstOrDefault();
            if (checkMeta == null)
            {
                MetaData meta = new MetaData();
                DateTime dt = System.DateTime.Now;

                meta.licence_id = id;
                meta.metadata_MachineID = machine_Id;
                meta.metadata_LastName = ln.ToUpper();
                meta.metatdata_FirstName = char.ToUpper(fn[0]) + fn.Substring(1);
                meta.metadata_modules = serial;
                meta.metadata_company = company;
                meta.metadata_phone = phone_number;
                meta.metadata_mail = email_address;
                meta.date_Activated = dt;
                meta.metadata_option1 = op1;
                meta.metadata_option2 = op2;
                meta.metadata_option3 = op3;

                _db.MetaDatas.AddObject(meta);
                _db.SaveChanges();

            }
            else 
            {
                checkMeta.metadata_id = checkMeta.metadata_id;
                checkMeta.licence_id = id;
                checkMeta.metadata_MachineID = machine_Id;
                checkMeta.metadata_LastName = ln.ToUpper();
                checkMeta.metatdata_FirstName = char.ToUpper(fn[0]) + fn.Substring(1);
                checkMeta.metadata_modules = serial;
                checkMeta.metadata_company = company;
                checkMeta.metadata_phone = phone_number;
                checkMeta.metadata_mail = email_address;
                checkMeta.metadata_option1 = op1;
                checkMeta.metadata_option2 = op2;
                checkMeta.metadata_option3 = op3;

                _db.SaveChanges();

            }
            
       }

        public int checkMetaData(int id)
        {

            var checkMeta = _db.MetaDatas.Where(x => x.licence_id == id).FirstOrDefault();
            if (checkMeta == null)
            {
               return 0;
            }
            else
            {
                return 1;
            }

        }

        public int check_MachineId(int id, string machineID)
        {
            int isUse = 0;

            var macId = _db.MetaDatas.Where(x => x.metadata_MachineID == machineID && x.licence_id == id).FirstOrDefault();

            if (macId == null)
            {
                isUse = 0;
            }
            else 
            {
                isUse = 1;
            }

            return isUse;
        }

        public string deserializeHere(string ako)
        {
            LicenceInfo li = LicenceInfo.Deserialize(ako);
            string expected = li.Serialize();

            return expected;
        }
        
    }
}
